# Untangled Conditionals Kata


## introduction

Ce projet est un exercice de refactoring existant dans d'autres langages : 

- Le projet initial : https://github.com/tomphp/untangled-conditionals-kata

Je l'ai porté en WinDev 25 pour le proposer en exercice aux personnes que je coache. Je le mets à disposition de la communauté pour que vous puissiez vous améliorer.


## Présentation

Ce projet contient le code initial. La logique est contenue dans une seule méthode, la méthode run de la classe cPipeline. Elle contient un certain nombre de if imbriqué. L'objectif est de refactoriser le code pour obtenir un code plus lisible.

Rappelez-vous, les tests doivent être au vert en permanence.

## Pré-requis
wxUnitViewer doit être installé et configuré sur le poste. Pour cela, vous pouvez lire la [description de wxUnitViewer](https://gitlab.com/Johjo/wxunitviewer).

## Comment utiliser ce projet

* Le moyen le plus simple est de cloner ce projet via git ou de télécharger le zip.
* [Télécharger le zip](https://gitlab.com/Johjo/untangled-conditionals-kata-wd25/-/archive/master/untangled-conditionals-kata-wd25-master.zip)
* Ouvrez le projet sous WinDev 25 minimum.
* Enfin, executez les tests en faisant un GO du projet (Ctrl + F9) et vérifiez que wxUnitViewer s'affiche correctement avec un test qui réussit et un test qui échoue.





